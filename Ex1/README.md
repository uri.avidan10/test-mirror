# this product contains:

http server on port 8000, that serves the file file.doc, under documents (of the user that runs the current thread).
In case this file does not exist, its backup file is given- C:\\file-backup.doc.
In case they both don't exist- return 404 not found.

To terminate the server, call Get with the following path: 11AED76A-CA47-45C5-A665-95D92B5C70C6.
Example: curl http://x.x.x.x:8000/11AED76A-CA47-45C5-A665-95D92B5C70C6

# this product does not contain:

- support for obtaining any other file.
- providing any metadata about the given files.
- support for handling large files (the files are given in one chunk, uncomprssed).
- support for multiple clients at once.
- support for methods other than GET.

# known issues:

- the server does not check that port 8000 is free before using it.