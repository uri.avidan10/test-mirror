#pragma once

#include "SessionSocket.hpp"
#include <string>

class Server final
{
public:
	static void run();

public:
	Server() = delete;
	~Server() = delete;

private:
	enum class ServerStatus
	{
		RUN,
		QUIT
	};

private:
	static ServerStatus handle_session(SessionSocket& session_socket);
	static std::wstring get_special_file_location();
};

