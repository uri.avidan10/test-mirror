#include "Server.hpp"
#include "Config.hpp"
#include "Exceptions.hpp"
#include "File.hpp"
#include "HttpUtils.hpp"
#include "ListenSocket.hpp"
#include "Misc.hpp"
#include "WSA.hpp"
#include <sstream>

void Server::run()
{
	const WSA wsa;
	ListenSocket server(SERVER_PORT);

	ServerStatus server_status = ServerStatus::RUN;

	while (server_status != ServerStatus::QUIT)
	{
		SessionSocket session_socket = server.accept_client();

		server_status = handle_session(session_socket);
	}
}

Server::ServerStatus Server::handle_session(SessionSocket& session_socket)
{

	while (session_socket.is_connection_up())
	{
		try
		{
			static constexpr uint32_t CHUNK_SIZE = 1024;
			const std::vector<uint8_t> raw_request = session_socket.recv_data(CHUNK_SIZE);

			std::wstring path_in_request = HttpUtils::get_file_path_of_http_get_request(raw_request);

			if (path_in_request == QUIT_PATH)
			{
				return ServerStatus::QUIT;
			}

			static constexpr const wchar_t* const IDENTIFIER = L"/file";
			const std::wstring special_file_location = get_special_file_location();

			if (path_in_request == IDENTIFIER && File::does_file_exist(special_file_location))
			{
				session_socket.send_data(HttpUtils::create_get_response(special_file_location));
				continue;
			}

			if (path_in_request == IDENTIFIER && File::does_file_exist(BACKUP_FILE_LOCATION))
			{
				session_socket.send_data(HttpUtils::create_get_response(BACKUP_FILE_LOCATION));
				continue;
			}

			session_socket.send_data(HttpUtils::create_not_found_response());
			
		}
		catch (const WSAException& e)
		{
			if (e.get_wsa_error() == WSAECONNRESET ||
				e.get_wsa_error() == WSAECONNABORTED)
			{
				break;
			}
			throw;
		}
		catch (const Exception& e)
		{
			if (e.get_error_code() ==
				static_cast<uint32_t>(ErrorCodes::CONNECTION_IS_DOWN) ||
				e.get_error_code() == 
				static_cast<uint32_t>(ErrorCodes::DATA_NOT_IN_HTTP_GET_FORMAT))
			{
				break;
			}
			throw;
		}
	}

	return ServerStatus::RUN;
}

std::wstring Server::get_special_file_location()
{
	std::wstringstream stream;

	static constexpr const wchar_t* const USERS_PATH = L"C:\\Users\\";
	stream << USERS_PATH;
	stream << Misc::get_current_username();

	static constexpr const wchar_t* const FILE_PATH_IN_DOCUMENTS = L"\\Documents\\file.doc";
	stream << FILE_PATH_IN_DOCUMENTS;

	return stream.str();
}
