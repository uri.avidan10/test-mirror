#pragma once

#include <cstdint>

static constexpr uint16_t SERVER_PORT = 8000;

static constexpr const wchar_t* const BACKUP_FILE_LOCATION = L"C:\\file-backup.doc";

static constexpr const wchar_t* const QUIT_PATH = L"/11AED76A-CA47-45C5-A665-95D92B5C70C6";
