#pragma once
#include <string>
#include <vector>

class HttpUtils final
{
public:
	static std::wstring get_file_path_of_http_get_request(const std::vector<uint8_t>& raw_request);
	static std::vector<uint8_t> create_get_response(const std::wstring& file_path);
	static std::vector<uint8_t> create_not_found_response();

public:
	HttpUtils() = delete;
	~HttpUtils() = delete;

private:
	static std::wstring extract_request_line_only(const std::vector<uint8_t>& raw_request);
};

