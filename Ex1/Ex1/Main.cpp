#include "Socket.hpp"
#include "CmdArgParser.hpp"
#include "Debug.hpp"
#include "Exceptions.hpp"
#include "File.hpp"
#include "Server.hpp"
#include <Windows.h>

int WINAPI wWinMain(
	HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	PWSTR lpCmdLine,
	int nCmdShow)
{
	try
	{
		UNREFERENCED_PARAMETER(hInstance);
		UNREFERENCED_PARAMETER(hPrevInstance);
		UNREFERENCED_PARAMETER(lpCmdLine);
		UNREFERENCED_PARAMETER(nCmdShow);
		
		Server::run();

		return EXIT_SUCCESS;
	}
	catch (const WinApiException& e)
	{
		UNREFERENCED_PARAMETER(e);
		TRACE(
			L"Got WinApiException! Error code: ",
			e.get_error_code(),
			L" , Last error: ",
			e.get_last_error(),
			L"\n");
	}
	catch (const WSAException& e)
	{
		UNREFERENCED_PARAMETER(e);
		TRACE(
			L"Got WSAException! Error code: ",
			e.get_error_code(),
			L" , Wsa error: ",
			e.get_wsa_error(),
			L"\n");
	}
	catch (const Exception& e)
	{
		UNREFERENCED_PARAMETER(e);
		TRACE(
			L"Got Exception! Error code: ",
			e.get_error_code(),
			L"\n");
	}
	catch (...)
	{
		TRACE(L"Got an unkown Exception!\n");
	}
	return EXIT_FAILURE;
}
