#include "HttpUtils.hpp"
#include "Exceptions.hpp"
#include "Debug.hpp"
#include "File.hpp"
#include "StringUtils.hpp"
#include <string>

enum class HttpRequestLineFormat : uint32_t
{
	ACTION,
	PATH,
	VERSION,
	AMOUNT_OF_PARTS
};

std::wstring HttpUtils::get_file_path_of_http_get_request(const std::vector<uint8_t>& raw_request)
{
	const std::wstring request_line = extract_request_line_only(raw_request);

	static constexpr wchar_t SPACE = L' ';
	const std::vector<std::wstring> request_line_parts = StringUtils::split_string(request_line, SPACE);

	static constexpr const wchar_t* const GET_ACTION = L"GET";
	static constexpr const wchar_t* const HTTP_VERSION_PREFIX = L"HTTP/";
	static constexpr size_t START_INDEX = 0;

	if (request_line_parts.size() != static_cast<uint32_t>(HttpRequestLineFormat::AMOUNT_OF_PARTS) ||
		request_line_parts[static_cast<uint32_t>(HttpRequestLineFormat::ACTION)] != GET_ACTION ||
		request_line_parts[static_cast<uint32_t>(HttpRequestLineFormat::VERSION)]
		.find(HTTP_VERSION_PREFIX) != START_INDEX)
	{
		TRACE(request_line_parts[static_cast<uint32_t>(HttpRequestLineFormat::ACTION)]);
		throw Exception(ErrorCodes::DATA_NOT_IN_HTTP_GET_FORMAT);
	}

	return request_line_parts[static_cast<uint32_t>(HttpRequestLineFormat::PATH)];
}

std::vector<uint8_t> HttpUtils::create_get_response(const std::wstring& file_path)
{
	File file(file_path, FileTypes::READ);
	const std::vector<uint8_t> raw_file_data = file.read(file.get_size());

	std::vector<uint8_t> response;

	std::string prefix;
	prefix += "HTTP/1.0 200 OK\r\n";
	prefix += "Server: localhost\r\n";
	prefix += "Content-Length: ";
	prefix += std::to_string(raw_file_data.size()) + "\r\n\r\n";

	response.insert(response.end(), prefix.begin(), prefix.end());
	response.insert(response.end(), raw_file_data.begin(), raw_file_data.end());

	return response;
}

std::wstring HttpUtils::extract_request_line_only(const std::vector<uint8_t>& raw_request)
{
	const std::string request_as_string = std::string(raw_request.begin(), raw_request.end());

	static constexpr const char* const WINDOWS_END_OF_LINE = "\r\n";
	const size_t end_of_first_line = request_as_string.find(WINDOWS_END_OF_LINE);

	if (end_of_first_line == std::string::npos)
	{
		throw Exception(ErrorCodes::DATA_NOT_IN_HTTP_GET_FORMAT);
	}

	const std::string request_line_only(
		request_as_string.begin(),
		request_as_string.begin() + end_of_first_line);

	return StringUtils::ansi_to_wide(request_line_only);
}

std::vector<uint8_t> HttpUtils::create_not_found_response()
{
	std::string prefix;
	prefix += "HTTP/1.0 404 FILE NOT FOUND\r\n";
	prefix += "Server: localhost\r\n";
	prefix += "Content-Length: 0\r\n\r\n";

	return std::vector<uint8_t>(prefix.begin(), prefix.end());
}
