#pragma once

#include "Socket.hpp"
#include <vector>

class SessionSocket final : public Socket
{
public:
	explicit SessionSocket(const SOCKET session_socket);

public:
	bool is_connection_up() const;
	std::vector<uint8_t> recv_data(const uint32_t max_bytes_to_recv);
	void send_data(const std::vector<uint8_t>& data);

public:
	~SessionSocket() override = default;
	SessionSocket(const SessionSocket&) = delete;
	SessionSocket& operator=(const SessionSocket&) = delete;

private:
	bool _is_connection_up;
};

