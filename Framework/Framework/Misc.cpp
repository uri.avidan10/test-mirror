#include "Misc.hpp"
#include "Exceptions.hpp"
#include "StringUtils.hpp"
#include <Windows.h>

std::wstring Misc::get_current_username()
{
	uint32_t buffer_size = 1;

	static constexpr uint32_t DOUBLE_SIZE = 2;
	static constexpr uint32_t MAX_TRIES = 30;
	for (uint32_t i = 0; i < MAX_TRIES; ++i, buffer_size *= DOUBLE_SIZE)
	{
		std::wstring result(buffer_size, StringUtils::NULL_TERMINATOR);

		DWORD username_size = buffer_size;
		if (::GetUserNameW(__out result.data(), __out &username_size))
		{
			static constexpr uint32_t IGNORE_NULL_TERMINATOR = 1;
			result.resize(username_size - IGNORE_NULL_TERMINATOR);
			return result;
		}

		if (::GetLastError() != ERROR_INSUFFICIENT_BUFFER)
		{
			throw WinApiException(ErrorCodes::GET_USER_NAME_FAILED);
		}
	}

	throw Exception(ErrorCodes::FAILED_GETTING_USERNAME);
}
