﻿#pragma once

#include <cstdint>

#ifndef _DEBUG
#define Exception gf43743fd743sd
#define WinApiException asdf7q732sd
#endif


enum class ErrorCodes : uint32_t
{
	COMMAND_LINE_TO_ARGV_FAILED,
	OUT_OF_BOUNDS,
	WRONG_NUMBER_OF_ARGS,
	CREATE_FILE_FAILED,
	READ_FILE_FAILED,
	READ_ZERO_BYTES,
	WRITTEN_ZERO_BYTES,
	GET_FILE_SIZE_FAILED,
	CANNOT_CONVERT_VECTOR_TO_STRING,
	WSA_STARTUP_FAILED,
	WSA_HAS_OLD_VERSION,
	GET_ADDR_INFO_FAILED,
	WSA_SOCKET_FAILED,
	WSA_BIND_FAILED,
	WSA_LISTEN_FAILED,
	WSA_ACCEPT_FAILED,
	CONNECTION_IS_DOWN,
	WSA_RECV_FAILED,
	WSA_SEND_FAILED,
	WSA_SENT_ZERO_BYTES,
	CANNOT_CONVERT_ANSI_TO_WIDE,
	DATA_NOT_IN_HTTP_GET_FORMAT,
	GET_USER_NAME_FAILED,
	FAILED_GETTING_USERNAME,
};

class Exception
{
public:
	explicit Exception(const ErrorCodes error_code);

public:
	uint32_t get_error_code() const;

public:
	virtual ~Exception() = default;
	Exception(const Exception&) = delete;
	Exception& operator=(const Exception&) = delete;

private:
	const uint32_t _error_code;
};

class WinApiException final : public Exception
{
public:
	explicit WinApiException(const ErrorCodes error_code);

public:
	uint32_t get_last_error() const;

public:
	~WinApiException() override = default;
	WinApiException(const WinApiException&) = delete;
	WinApiException& operator=(const WinApiException&) = delete;

private:
	const uint32_t _last_error;
};

class WSAException final : public Exception
{
public:
	explicit WSAException(const ErrorCodes error_code);

public:
	uint32_t get_wsa_error() const;

public:
	~WSAException() override = default;
	WSAException(const WinApiException&) = delete;
	WSAException& operator=(const WSAException&) = delete;

private:
	const uint32_t _wsa_error;
};
