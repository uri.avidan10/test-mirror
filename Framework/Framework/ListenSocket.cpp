#include "ListenSocket.hpp"
#include "Debug.hpp"
#include "Exceptions.hpp"
#include <string>
#include <ws2def.h>
#include <ws2tcpip.h>

ListenSocket::ListenSocket(const uint16_t port) :
	Socket(),
	_address_info(get_address_info(port))
{
	bind_to_address();
	enter_listen_mode();
}

ListenSocket::~ListenSocket()
{
	try
	{
		freeaddrinfo(_address_info);
	}
	catch (...)
	{
		TRACE(L"Unkown Exception in NAME DTOR");
	}
}

SessionSocket ListenSocket::accept_client()
{
	static constexpr sockaddr* IGNORE_ADDRESS = nullptr;
	static constexpr int* IGNORE_ADDRESS_LENGTH = nullptr;

	const SOCKET session_socket = ::accept(
		_socket,
		IGNORE_ADDRESS,
		IGNORE_ADDRESS_LENGTH);

	if (session_socket == INVALID_SOCKET)
	{
		throw WSAException(ErrorCodes::WSA_ACCEPT_FAILED);
	}

	return SessionSocket(session_socket);
}

addrinfo* ListenSocket::get_address_info(const uint16_t port)
{
	addrinfo socket_settings{};
	socket_settings.ai_family = AF_INET;
	socket_settings.ai_socktype = SOCK_STREAM;
	socket_settings.ai_protocol = IPPROTO_TCP;
	socket_settings.ai_flags = AI_PASSIVE;

	static constexpr PCSTR DEFAULT_NAME = nullptr;

	addrinfo* address_info = nullptr;

	const int get_address_info_result = ::getaddrinfo(
		DEFAULT_NAME,
		std::to_string(port).c_str(),
		&socket_settings,
		__out &address_info);

	static constexpr int GET_ADDR_INFO_SUCCEEDED = 0;
	if (get_address_info_result != GET_ADDR_INFO_SUCCEEDED)
	{
		TRACE(L"getaddrinfo failed. return code: ", get_address_info_result, L'\n');
		throw Exception(ErrorCodes::GET_ADDR_INFO_FAILED);
	}

	return address_info;
}

void ListenSocket::bind_to_address()
{
	const int result = ::bind(
		_socket,
		_address_info->ai_addr,
		static_cast<int>(_address_info->ai_addrlen));

	if (result == SOCKET_ERROR)
	{
		throw WSAException(ErrorCodes::WSA_BIND_FAILED);
	}
}

void ListenSocket::enter_listen_mode()
{
	static constexpr int BACKLOG_SIZE = 5;

	const int result = ::listen(
		_socket,
		BACKLOG_SIZE);

	if (result == SOCKET_ERROR)
	{
		throw WSAException(ErrorCodes::WSA_LISTEN_FAILED);
	}
}
