#pragma once

#include "SessionSocket.hpp"
#include "Socket.hpp"
#include <cstdint>

class ListenSocket final : public Socket
{
public:
	explicit ListenSocket(const uint16_t port);
	~ListenSocket() override;

public:
	SessionSocket accept_client();

public:
	ListenSocket(const ListenSocket&) = delete;
	ListenSocket& operator=(const ListenSocket&) = delete;

private:
	static addrinfo* get_address_info(const uint16_t port);

private:
	void bind_to_address();
	void enter_listen_mode();

private:
	addrinfo* const _address_info;
};

