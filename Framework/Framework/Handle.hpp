#pragma once

#include <Windows.h>

class Handle
{
public:
	explicit Handle(const HANDLE handle);
	virtual ~Handle();

public:
	Handle(const Handle&) = delete;
	Handle& operator=(const Handle&) = delete;

protected:
	const HANDLE _handle;
};

