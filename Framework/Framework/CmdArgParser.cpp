#include "CmdArgParser.hpp"
#include "Debug.hpp"
#include "Exceptions.hpp"
#include <Windows.h>


CmdArgParser::CmdArgParser(const int expected_argc) :
	_argc(expected_argc),
	_argv(get_argv(expected_argc))
{
}

CmdArgParser::~CmdArgParser()
{
	try
	{
		free_argv_best_effort(_argv);
	}
	catch (...)
	{
	}
}

uint32_t CmdArgParser::get_argc() const
{
	return _argc;
}

std::wstring CmdArgParser::get_arg(const uint32_t index) const
{
	if (index < _argc)
	{
		return std::wstring(_argv[index]);
	}

	throw Exception(ErrorCodes::OUT_OF_BOUNDS);
}

wchar_t** CmdArgParser::get_argv(const int expected_argc)
{
	int argc = 0;
	const LPWSTR command_line = ::GetCommandLineW();
	wchar_t** argv = ::CommandLineToArgvW(
		command_line,
		__out &argc
	);

	if (argv == nullptr)
	{
		throw WinApiException(ErrorCodes::COMMAND_LINE_TO_ARGV_FAILED);
	}

	if (argc != expected_argc)
	{
		free_argv_best_effort(argv);
		throw Exception(ErrorCodes::WRONG_NUMBER_OF_ARGS);
	}

	return argv;
}

void CmdArgParser::free_argv_best_effort(wchar_t** const argv)
{
	static constexpr HLOCAL LOCAL_FREE_SUCCEEDED = nullptr;

	if (::LocalFree(argv) != LOCAL_FREE_SUCCEEDED)
	{
		TRACE(L"LocalFree failed, last error: ", ::GetLastError(), L"\n");
	}
}
