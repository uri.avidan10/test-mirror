#include "Socket.hpp"
#include "Debug.hpp"
#include "Exceptions.hpp"
#include <winsock2.h>

Socket::Socket() :
	_socket(get_socket())
{
	
}

Socket::Socket(const SOCKET socket) :
	_socket(socket)
{
}

Socket::~Socket()
{
	try
	{
		if (::shutdown(_socket, SD_BOTH) == SOCKET_ERROR)
		{
			TRACE(L"shutdown failed in Socket DTOR. wsa error: ", ::WSAGetLastError(), L'\n');
		}

		if (::closesocket(_socket) == SOCKET_ERROR)
		{
			TRACE(L"closesocket failed in Socket DTOR. wsa error: ", ::WSAGetLastError(), L'\n');
		}
	}
	catch (...)
	{
		TRACE(L"Unkown Exception in Socket DTOR");
	}
}

SOCKET Socket::get_socket()
{
	const SOCKET socket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (socket == INVALID_SOCKET)
	{
		throw WSAException(ErrorCodes::WSA_SOCKET_FAILED);
	}

	return socket;
}
