#pragma once

#include <Windows.h>
#include <sstream>
#include <string>

class Trace final
{
public:
	template<typename... Args>
	static void trace(Args... args);

public:
	Trace() = delete;
	~Trace() = delete;
};

template <typename ... Args>
void Trace::trace(Args... args)
{
	try
	{
		std::wstringstream stream;
		(stream << ... << args);
		OutputDebugStringW(stream.str().c_str());
	}
	catch (...)
	{
	}
}
