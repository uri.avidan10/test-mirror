#pragma once
#include <string>

class Misc final
{
public:
	static std::wstring get_current_username();

public:
	Misc() = delete;
	~Misc() = delete;
};

