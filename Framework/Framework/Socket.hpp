#pragma once

#define WIN32_LEAN_AND_MEAN
#include <winsock2.h>

class Socket
{
public:
	explicit Socket();
	explicit Socket(const SOCKET socket);
	virtual ~Socket();

public:
	Socket(const Socket&) = delete;
	Socket& operator=(const Socket&) = delete;

private:
	static SOCKET get_socket();

protected:
	const SOCKET _socket;
};

