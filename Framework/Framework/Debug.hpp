#pragma once

#ifdef _DEBUG

#include "Trace.hpp"
#define TRACE(...)														\
do																		\
{																		\
	Trace::trace(L">> File: ", __FILE__, L", Line: ", __LINE__, L"\n");	\
	Trace::trace(__VA_ARGS__);											\
}																		\
while (false)
#define ASSERT(condition, ...) do { if (!condition) { TRACE(__VA_ARGS__); } } while (false)

#else

#define TRACE(...) do {} while (false)
#define ASSERT(condition, ...) do {} while (false)

#endif
