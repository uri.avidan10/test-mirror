﻿#include "Exceptions.hpp"
#include <Windows.h>

Exception::Exception(const ErrorCodes error_code) :
	_error_code(static_cast<uint32_t>(error_code))
{
}

uint32_t Exception::get_error_code() const
{
	return _error_code;
}

WinApiException::WinApiException(const ErrorCodes error_code) :
	Exception(error_code),
	_last_error(::GetLastError())
{
}

uint32_t WinApiException::get_last_error() const
{
	return _last_error;
}

WSAException::WSAException(const ErrorCodes error_code) :
	Exception(error_code),
	_wsa_error(::WSAGetLastError())
{
}

uint32_t WSAException::get_wsa_error() const
{
	return _wsa_error;
}
