#pragma once
#include <cstdint>
#include <string>

class CmdArgParser final
{
public:
	explicit CmdArgParser(const int expected_argc);
	~CmdArgParser();

public:
	uint32_t get_argc() const;
	std::wstring get_arg(const uint32_t index) const;

public:
	CmdArgParser(const CmdArgParser&) = delete;
	CmdArgParser& operator=(const CmdArgParser&) = delete;

private:
	static wchar_t** get_argv(const int expected_argc);
	static void free_argv_best_effort(wchar_t** const argv);

private:
	const uint32_t _argc;
	wchar_t** _argv;
};

