#include "SessionSocket.hpp"
#include "Exceptions.hpp"

SessionSocket::SessionSocket(const SOCKET session_socket) :
	Socket(session_socket),
	_is_connection_up(true)
{
}

bool SessionSocket::is_connection_up() const
{
	return _is_connection_up;
}

std::vector<uint8_t> SessionSocket::recv_data(const uint32_t max_bytes_to_recv)
{
	if (!_is_connection_up)
	{
		throw Exception(ErrorCodes::CONNECTION_IS_DOWN);
	}

	std::vector<uint8_t> buffer(max_bytes_to_recv);

	static constexpr int NO_SPECIAL_RECV_FLAGS = 0;
	const int recv_result = ::recv(
		_socket,
		__out reinterpret_cast<char*>(buffer.data()),
		static_cast<int>(buffer.size()),
		NO_SPECIAL_RECV_FLAGS
	);

	if (recv_result == SOCKET_ERROR)
	{
		throw WSAException(ErrorCodes::WSA_RECV_FAILED);
	}

	buffer.resize(recv_result);

	if (recv_result == 0)
	{
		_is_connection_up = false;
	}

	return buffer;
}

void SessionSocket::send_data(const std::vector<uint8_t>& data)
{
	if (!_is_connection_up)
	{
		throw Exception(ErrorCodes::CONNECTION_IS_DOWN);
	}

	int bytes_sent_so_far = 0;

	while (bytes_sent_so_far < static_cast<int>(data.size()))
	{
		static constexpr int NO_SPECIAL_SEND_FLAGS = 0;
		const int send_result = ::send(
			_socket,
			bytes_sent_so_far + reinterpret_cast<const char*>(data.data()),
			static_cast<int>(data.size()) - bytes_sent_so_far,
			NO_SPECIAL_SEND_FLAGS
		);

		if (send_result == SOCKET_ERROR)
		{
			throw WSAException(ErrorCodes::WSA_SEND_FAILED);
		}

		if (send_result == 0)
		{
			throw Exception(ErrorCodes::WSA_SENT_ZERO_BYTES);
		}

		bytes_sent_so_far += send_result;
	}
}
