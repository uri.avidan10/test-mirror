#include "Handle.hpp"
#include "Debug.hpp"

Handle::Handle(const HANDLE handle) :
	_handle(handle)
{
}

Handle::~Handle()
{
	try
	{
		if (!::CloseHandle(_handle))
		{
			TRACE(L"CloseHandle failed\n");
		}
	}
	catch (...)
	{
		TRACE(L"Unkown Exception in Handle DTOR");
	}
}
