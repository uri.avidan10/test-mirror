#pragma once

#include <cstdint>
#include <string>
#include <vector>

class StringUtils final
{
public:
	static constexpr wchar_t NULL_TERMINATOR = L'\0';

public:
	static std::vector<uint8_t> string_to_vector(const std::wstring& str);
	static std::wstring vector_to_string(const std::vector<uint8_t>& vec);
	static std::vector<std::wstring> split_string(const std::wstring& str, const wchar_t separator);
	static std::wstring ansi_to_wide(const std::string& str);

public:
	StringUtils() = delete;
	~StringUtils() = delete;
};

