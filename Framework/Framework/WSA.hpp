#pragma once

class WSA final
{
public:
	explicit WSA();
	~WSA();

public:
	WSA(const WSA&) = delete;
	WSA& operator=(const WSA&) = delete;

private:
	static void wsa_cleanup_best_effort();
};

