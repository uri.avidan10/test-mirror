#define WIN32_LEAN_AND_MEAN

#include "WSA.hpp"
#include "Debug.hpp"
#include "Exceptions.hpp"
#include <windows.h>
#include <winsock2.h>

WSA::WSA()
{
	static constexpr WORD LATEST_WSA_VERSION = MAKEWORD(2, 2);
    WSADATA wsa_data{};

    const int startup_result = ::WSAStartup(LATEST_WSA_VERSION, &wsa_data);

	static constexpr int SUCCESSFUL_STARTUP = 0;
    if (startup_result != SUCCESSFUL_STARTUP) 
	{
        TRACE(L"WSAStartup failed. Return code: ", startup_result, L'\n');
        throw Exception(ErrorCodes::WSA_STARTUP_FAILED);
    }

    if (wsa_data.wVersion != LATEST_WSA_VERSION)
    {
		wsa_cleanup_best_effort();
		throw Exception(ErrorCodes::WSA_HAS_OLD_VERSION);
    }
}

WSA::~WSA()
{
	try
	{
		wsa_cleanup_best_effort();
	}
	catch (...)
	{
		TRACE(L"Unkown Exception in WSA DTOR");
	}
}

void WSA::wsa_cleanup_best_effort()
{
	if (::WSACleanup() == SOCKET_ERROR)
	{
		TRACE(L"WSACleanup failed. wsa error: ", ::WSAGetLastError(), L'\n');
	}
}