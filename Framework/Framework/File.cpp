#include "File.hpp"
#include "Exceptions.hpp"

bool File::does_file_exist(const std::wstring& path)
{
	const DWORD attributes = GetFileAttributesW(path.c_str());

	return attributes != INVALID_FILE_ATTRIBUTES;
}

File::File(const std::wstring& path, const FileTypes file_type) :
	Handle(open_file(path, file_type))
{
}

void File::write(const std::wstring& str)
{
	const uint32_t bytes_to_write = str.size() * sizeof(wchar_t);

	uint32_t bytes_written_so_far = 0;
	while (bytes_written_so_far < bytes_to_write)
	{
		static constexpr LPOVERLAPPED NO_ASYNC_IO = nullptr;
		DWORD bytes_written = 0;

		const BOOL written_successfully = ::WriteFile(
			_handle,
			reinterpret_cast<const uint8_t*>(str.data()) + bytes_written_so_far,
			bytes_to_write - bytes_written_so_far,
			__out &bytes_written,
			NO_ASYNC_IO
		);

		if (!written_successfully)
		{
			throw WinApiException(ErrorCodes::READ_FILE_FAILED);
		}

		if (bytes_written == 0)
		{
			throw Exception(ErrorCodes::WRITTEN_ZERO_BYTES);
		}

		bytes_written_so_far += bytes_written;
	}
}

std::vector<uint8_t> File::read(const uint32_t amount)
{
	std::vector<uint8_t> result(amount);

	uint32_t bytes_read_so_far = 0;
	while (bytes_read_so_far < amount)
	{
		static constexpr LPOVERLAPPED NO_ASYNC_IO = nullptr;
		DWORD bytes_read = 0;

		const BOOL read_succeeded = ::ReadFile(
			_handle,
			reinterpret_cast<uint8_t*>(result.data()) + bytes_read_so_far,
			amount - bytes_read_so_far,
			__out &bytes_read,
			NO_ASYNC_IO
		);

		if (!read_succeeded)
		{
			throw WinApiException(ErrorCodes::READ_FILE_FAILED);
		}

		if (bytes_read == 0)
		{
			throw Exception(ErrorCodes::READ_ZERO_BYTES);
		}

		bytes_read_so_far += bytes_read;
	}

	return result;
}

uint32_t File::get_size() const
{
	static constexpr LPDWORD ASSUME_SIZE_FITS_DWORD = nullptr;

	const DWORD file_size = ::GetFileSize(
		_handle,
		ASSUME_SIZE_FITS_DWORD
	);

	if (file_size == INVALID_FILE_SIZE)
	{
		throw WinApiException(ErrorCodes::GET_FILE_SIZE_FAILED);
	}

	return file_size;
}

HANDLE File::open_file(const std::wstring& path, const FileTypes file_type)
{
	static constexpr LPSECURITY_ATTRIBUTES DEFAULT_SECURITY = nullptr;
	static constexpr HANDLE NO_TEMP_FILE = nullptr;
	const HANDLE handle = ::CreateFileW(
		path.c_str(),
		file_type == FileTypes::WRITE ? GENERIC_WRITE : GENERIC_READ,
		FILE_SHARE_READ,
		DEFAULT_SECURITY,
		file_type == FileTypes::WRITE ? CREATE_NEW : OPEN_EXISTING,
		FILE_ATTRIBUTE_HIDDEN,
		NO_TEMP_FILE);

	if (handle == INVALID_HANDLE_VALUE)
	{
		throw WinApiException(ErrorCodes::CREATE_FILE_FAILED);
	}

	return handle;
}
