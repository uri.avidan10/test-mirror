#include "StringUtils.hpp"
#include "Exceptions.hpp"
#include <sstream>
#include <Windows.h>

std::vector<uint8_t> StringUtils::string_to_vector(const std::wstring& str)
{
	return std::vector<uint8_t>(
		reinterpret_cast<const uint8_t*>(str.data()),
		reinterpret_cast<const uint8_t*>(str.data()) + str.size() * sizeof(wchar_t));
}

std::wstring StringUtils::vector_to_string(const std::vector<uint8_t>& vec)
{
	if (vec.size() % sizeof(wchar_t) != 0)
	{
		throw Exception(ErrorCodes::CANNOT_CONVERT_VECTOR_TO_STRING);
	}

	return std::wstring(
		reinterpret_cast<const wchar_t*>(vec.data()),
		reinterpret_cast<const wchar_t*>(vec.data()) + vec.size() / sizeof(wchar_t));
}

std::vector<std::wstring> StringUtils::split_string(const std::wstring& str, const wchar_t separator)
{
	std::vector<std::wstring> result;

	std::wstring next_string;
	std::wstringstream str_as_stream(str);
	while (std::getline(str_as_stream, next_string, separator))
	{
		result.push_back(next_string);
	}

	return result;
}

std::wstring StringUtils::ansi_to_wide(const std::string& str)
{
	std::wstring result(str.size(), NULL_TERMINATOR);

	const int conversion_result = ::MultiByteToWideChar(
		CP_ACP,
		MB_PRECOMPOSED,
		str.c_str(),
		str.size(),
		result.data(),
		result.size()
	);

	if (conversion_result != static_cast<int>(result.size()))
	{
		throw Exception(ErrorCodes::CANNOT_CONVERT_ANSI_TO_WIDE);
	}

	return result;
}
