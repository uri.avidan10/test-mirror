#pragma once

#include "Handle.hpp"
#include <string>
#include <vector>

enum class FileTypes
{
	WRITE = 0,
	READ
};

class File final : public Handle
{
public:
	static bool does_file_exist(const std::wstring& path);

public:
	explicit File(const std::wstring& path, const FileTypes file_type);

public:
	void write(const std::wstring& str);
	std::vector<uint8_t> read(const uint32_t amount);
	uint32_t get_size() const;

public:
	~File() override = default;
	File(const File&) = delete;
	File& operator=(const File&) = delete;

private:
	static HANDLE open_file(const std::wstring& path, const FileTypes file_type);
};
